// console.log(document)
// console.dir(document)
// console.log(document.domain)
// console.log(document.URL)
// console.log(document.all)
// console.log(document.all[3]);
// console.log(document.head)
// console.log(document.body)
// console.log(document.images)


// Selectors 

// console.log(document.getElementById('image'))
var image=document.getElementById('image')
// console.log(image)
// image.innerHTML='<p>image was here before</p>'
// image.style.borderBottom="2px solid black";
// image.style.width='500px';
var lists=document.getElementsByClassName('functions')
// console.log(lists)
// console.log(lists[1])
// lists[1].textContent='getElementsByClassName'
// lists[1].style.fontWeight='bold'

var li=document.getElementsByTagName('li')
 console.log(li)
// console.log(lists[2])
// lists[2].textContent='getElementsByTagName'
// lists[2].style.color='blue'

// Query selector 
// var para=document.querySelector('p')
// console.log(para)
// para.style.border='1px solid red'
var tree=document.querySelector('#image')
// console.log(tree)


// Query selectorAll 
var para=document.querySelectorAll('p')
// console.log(para)
// para[1].style.border='2px solid yellow'

//Traversing the DOM
// console.log(para[1].parentNode.parentNode)
// console.log(tree.parentNode)
// console.log(para[1].childNodes)



//Create Element

// create a div and inserting into body
var newDiv=document.createElement('div')
// var div=document.createElement('')
// newDiv.className=' newDiv'
// var image2=document.createElement('img')
// image2.setAttribute('src','./assets/images/image2.png')
// newDiv.appendChild(image2)
// document.body.appendChild(newDiv)
// console.log(newDiv)

//Event listener

// var event=document.createElement('div');
// event.className='event'
// event.style.textAlign='center'
// var input=document.createElement('input')
// input.setAttribute('type','number')
// input.setAttribute('placeholder','Enter number... ')
// input.setAttribute('id','number')
// event.appendChild(input)
// console.log(input)

// var output=document.createElement('span')
// output.setAttribute('id','output')
// output.style.margin='0 1em 0 1em'
// event.appendChild(output)

// var button=document.createElement('button')
// button.innerHTML='Square'
// button.addEventListener('click',calculateSquare)
// function calculateSquare(){
//     var passedNumber=document.getElementById('number').value;
//     const squaredNumber=passedNumber*passedNumber
//     output.innerHTML=squaredNumber
//     //console.log(squaredNumber)
// }
// // console.log(event)
// event.appendChild(button)
// document.body.appendChild(event)

// Other listeners
// 'mouseup'
// 'mousedown'
// 'mouseenter'
// 'mouseleave'
// 'mousemove' ...

//Event listener part 2

var event=document.createElement('div');
event.className='event'
event.style.textAlign='center'

var button=document.createElement('button')
button.innerHTML='Fetch Todo'
button.addEventListener('click',Task)

async function Task(){
    let tasks= await toDos();
    console.log(tasks)
}
function toDos(){
    return new Promise((resolve)=>{
        setTimeout(()=>{
            let todo={
                'task_1':'To complete callbacks',
                'task_2':'To complete promise',
                'task_3':'To complete async-await',
                'task_4':'To start with project'
            };
            resolve(todo)
        },2000)
    });
}

event.appendChild(button)
document.body.appendChild(event)



// arr={task_1: "To complete callbacks", task_2: "To complete promise", task_3: "To complete async-await", task_4: "To start with project"}
// function toDoLists(data){
//     var todo=document.createElement('div');
//     todo.className='event'
//     todo.style.textAlign='center'
//     text=document.createElement('textarea').value=data;
//     console.log(text)
//     // todo.appendChild(text)
//     document.body.appendChild(todo)
//     console.log()
// }
